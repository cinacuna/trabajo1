#include <pthread.h>
#include <stdio.h>
#include <malloc.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
struct Aut_en_Mov {
	int id;
	int dir;
	char *nombre;
};

// IMPLEMENTACION DE COLA OBTENIDO DE: https://gist.github.com/pfallasro/5847705

char* getPtrNom();
int getId();
int isEmpty();
int menu();
void clearBuffer();
void dequeue();
void enqueue();
void isEmptyWrap();

struct automov {
  int id;
  char *nombre;
  int dir;
	
  struct automov *siguiente;
};

typedef struct automov Nodo;
int cant=0;
Nodo *final;
Nodo *inicio;

int getId() {
  int num;
 // printf("Ingrese el ID: ");
  scanf("%d" , &num);
  return num;
}
int getDir() {
 int num;

  //printf("Ingrese la direccion: izq(1) o der(0)");


	scanf("%d" , &num);
   return num;
	
  
}

char* getPtrNom() {
  char d,*newAr;

  int i = 0;

  newAr = (char*) malloc(sizeof(char)*100);

 // printf("Ingrese el nombre: ");

  while((d = getchar()) != EOF && d != '\n') {
    newAr[i++] = d;
  }

  return newAr;
}


void dequeue() {
  Nodo *actual,
       *temporal;


  clearBuffer();

  if (isEmpty()) {
    
  } else {
    if (final == inicio) {

      printf("%d | %s\n", final->id, final->nombre);
    
      final = inicio = 0; 
    } else { 
    
      actual = final;
      while (actual != inicio) { 
        temporal = actual; 
        actual = temporal->siguiente;
      }
      printf("%d | %s\n", inicio->id, inicio->nombre); 
   
      inicio = temporal;
    }
	  cant--;
  }
}


void enqueue() {
  Nodo *nodoNuevo,
       *temporal;

  nodoNuevo = (Nodo*) malloc(sizeof(Nodo));
  clearBuffer();

  nodoNuevo->nombre = getPtrNom();
  nodoNuevo->id = getId();
   nodoNuevo->dir= getDir();
 
  if (isEmpty()) { 
    final = nodoNuevo; 
    inicio = nodoNuevo; 
  } else { 
    temporal = inicio; 
    inicio = nodoNuevo; 
    inicio->siguiente = temporal; 
  }
	cant++;
	

  clearBuffer();
}

int isEmpty() {
  if (!final) {
    return 1;
  } else {
    return 0;
  }
}

void clearBuffer() {
  while(getchar() != '\n')
    ;
}

pthread_mutex_t m = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t der_izq = PTHREAD_COND_INITIALIZER;
pthread_cond_t izq_der = PTHREAD_COND_INITIALIZER;
pthread_cond_t *cq[2] = { &der_izq, &izq_der };
/*pthread_cond_t cruz_der = PTHREAD_COND_INITIALIZER;
pthread_cond_t cruz_izq = PTHREAD_COND_INITIALIZER;
pthread_cond_t *cruz[2] = { &cruz_der, &cruz_izq };*/
char *sdir[] = { "derecha", "izquierda" };

int autms_on_bridge = 0;
int dir_on_bridge = -1; //-1 puente vacio
int pasaron[2]={0,0};
void *autm_thread (void *p) {
	struct Aut_en_Mov *autm = p;
	//VEHICULO LLEGA
	pthread_mutex_lock(&m);
	
	while (autms_on_bridge > 2 || (dir_on_bridge != -1 && dir_on_bridge != autm->dir)){
		pthread_cond_wait(cq[autm->dir], &m);
	}
	autms_on_bridge++;
	dir_on_bridge = autm->dir;
	
	printf("Auto %s , id %2d en puente, camino a %s, cant_autos en puente %d cautos (van a la %s)\n", autm->nombre, autm->id, sdir[autm->dir], autms_on_bridge, sdir[dir_on_bridge]);

	pthread_mutex_unlock(&m);
	//VEHICULO CRUSANDO
	sleep(4);
	
	pthread_mutex_lock(&m);
	//VEHICULO SALIENDO
	autms_on_bridge--;

	printf("Auto %s id %2d sale , camino a %s, cant_autos %d autos (van a la %s)\n",autm->nombre, autm->id, sdir[autm->dir], autms_on_bridge, sdir[dir_on_bridge]);

	if (autms_on_bridge > 0) {
		pthread_cond_signal(cq[autm->dir]);
	}
	else {
		dir_on_bridge = -1;
		pthread_cond_broadcast(cq[1 - autm->dir]);
	}
	
	pthread_mutex_unlock(&m);
	
	free(autm);
}
void run1(){
	pthread_t thr_id;
	pthread_attr_t attr;
	int autm_id = 0;
	struct Aut_en_Mov *autm;

	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
	int i=20;
	Nodo *actual = inicio;
		Nodo *temporal;
	while (actual!=NULL) {
		//cant--;
		autm = malloc(sizeof(struct Aut_en_Mov));
		autm->id = actual->id;
		autm->dir= actual->dir;
		autm->nombre =actual->nombre;
		printf(" %s llega de %s\n", actual->nombre, sdir[ autm->dir]);
		temporal=actual;
		actual=temporal->siguiente;
		pthread_create(&thr_id, &attr, autm_thread, (void *) autm);
		sleep(1);
	}

}

void run2(){
	pthread_t thr_id;
	pthread_attr_t attr;
	int autm_id = 0;
	struct Aut_en_Mov *autm;

	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);

	while (1) {
		autm = malloc(sizeof(struct Aut_en_Mov));
		autm->id = ++autm_id;
		autm->nombre = "-no name-" ;
		autm->dir = rand() & 1;
		
		printf("Auto %s, id %d llega de %s\n", autm->nombre, autm_id, sdir[1 - autm->dir]);
		
		pthread_create(&thr_id, &attr, autm_thread, (void *) autm);
		
		sleep(1);
		//system("clear");
	}

	

}
int main () {
	char menu_option;
	struct automov *actual;
	struct automov *temporal;
    do{
    scanf("%c",&menu_option);
	/*char newAr[10];
	fgets(newAr, 20, stdin);
	char *n="status";
	if(strcmp(newAr,"status")==0){
		
		menu_option='s';
	}*/
    switch(menu_option){

    case 's'://Muestra los automoviles con sus datos
			
        	
			actual=inicio;
			if(actual!=NULL){
				while(actual!=NULL){
					printf("Auto %s, id %d quiere ir hacia la %s\n", actual->nombre, actual->id , sdir[actual->dir]);
					temporal=actual;
					actual=temporal->siguiente;
				}
			}else{
					printf("No hay vehiculos\n");
			}
        break;
    case 'c':
    		enqueue();
        break;
    case'r':// utiliza los valores ingresados
        run1();
        break;
	case 't'://test - utiliza valores aleatorios
			run2();
			break;
    default:
      
            break;
    }

    }while(menu_option !='e');

	
	return 0;
}
